// SCIENCE //       http://newsapi.org/v2/top-headlines?country=se&category=science&apiKey=277c15c35d3a485099ee94bdb69c09dc
// HEALTH //        http://newsapi.org/v2/top-headlines?country=se&category=health&apiKey=116abdcbcd1f409db0e9378e4544cbf7
// BUSINESS //      http://newsapi.org/v2/top-headlines?country=se&category=business&apiKey=116abdcbcd1f409db0e9378e4544cbf7
// TECHNOLOGY //    http://newsapi.org/v2/top-headlines?country=se&category=technology&apiKey=116abdcbcd1f409db0e9378e4544cbf7
// SPORTS //        http://newsapi.org/v2/top-headlines?country=se&category=sports&apiKey=116abdcbcd1f409db0e9378e4544cbf7

// HTML
const elBtnScience = document.getElementById('btn-science');
const elBtnHealth = document.getElementById('btn-health');
const elBtnBusiness = document.getElementById('btn-busisess');
const elBtnTechnology = document.getElementById('btn-technology');
const elBtnSport = document.getElementById('btn-sport');
let list = document.getElementById("main-div-large");

let newsURL = 'http://newsapi.org/v2/top-headlines?country=se&category=science&apiKey=277c15c35d3a485099ee94bdb69c09dc';

// Buttons
// -------------------------------------------------------------------
elBtnScience.addEventListener('click', () => {
    console.log("click");
    newsURL = 'http://newsapi.org/v2/top-headlines?country=se&category=science&apiKey=277c15c35d3a485099ee94bdb69c09dc'
    while (list.hasChildNodes()) {  
        list.removeChild(list.firstChild);
        }
    init();
});
elBtnHealth.addEventListener('click', () => {
    console.log("click");
    newsURL = 'http://newsapi.org/v2/top-headlines?country=se&category=health&apiKey=116abdcbcd1f409db0e9378e4544cbf7'
    while (list.hasChildNodes()) {  
        list.removeChild(list.firstChild);
        }
    init();
});
elBtnBusiness.addEventListener('click', () => {
    console.log("click");
    newsURL = 'http://newsapi.org/v2/top-headlines?country=se&category=business&apiKey=116abdcbcd1f409db0e9378e4544cbf7'
    while (list.hasChildNodes()) {  
        list.removeChild(list.firstChild);
        }
    init();
});
elBtnTechnology.addEventListener('click', () => {
    console.log("click");
    newsURL = 'http://newsapi.org/v2/top-headlines?country=se&category=technology&apiKey=116abdcbcd1f409db0e9378e4544cbf7'
    while (list.hasChildNodes()) {  
        list.removeChild(list.firstChild);
        }
    init();
});
elBtnSport.addEventListener('click', () => {
    console.log("click");
    newsURL = 'http://newsapi.org/v2/top-headlines?country=se&category=sports&apiKey=116abdcbcd1f409db0e9378e4544cbf7'
    while (list.hasChildNodes()) {  
        list.removeChild(list.firstChild);
        }
    init();
});

// -------------------------------------------------------------------

init();
function init(){
    fetch(newsURL, {
        method: "GET",
        timeout: 5000
    })
    .then((response) => {
        if (response.ok) {
            console.log('OK')
        } else {
            console.log('NOT')
        }
        return response.json();

    })
    .then((data) => {
        const elMainDiv = document.getElementById("main-div-large");

        data.articles.forEach((article, index) => {
            // Create Article Element
            let element = document.createElement('article');
            // Set ID for Article Element
            // element.setAttribute("id", "main-div-new-"+index)
            element.id = "main-div-new-"+ (index);
            // Create Title in Article 
            let elArticleTitle = document.createElement("h1");
            elArticleTitle.innerText = article.title

            // Create background image in article
            //let elBackgroundUrl = document.getElementById(element.id);

            let aUrl = article.urlToImage; 
            if (aUrl == null) {
                console.log(element.id + 'no img exist');
            } else {
                element.style.backgroundImage = "url('"+ aUrl +"')";
                element.style.backgroundSize = "cover"
                
            }
            let siteUrl = article.url;
            element.addEventListener('click', ()=> {
                window.open(`${siteUrl}`);
            })

            // Appending
            
            element.appendChild(elArticleTitle);
            elMainDiv.appendChild(element);
        });
    })
    ;
}